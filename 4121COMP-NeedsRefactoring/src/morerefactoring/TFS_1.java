package morerefactoring;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * My TFS_1 class. Used to represent TFS_1s.
 * 
 * @author david
 */
public class TFS_1 {
	public String n;
	public int q;
	public double d;

	public static void main(String[] args) throws FileNotFoundException, IOException {
		FileInputStream fis = new FileInputStream("stock.txt");
		ArrayList<TFS_1> a = new ArrayList<>();

		Scanner scan = new Scanner(fis);

		while (scan.hasNextLine()) {
			TFS_1 t = new TFS_1();
			t.n = scan.nextLine();
			t.q = scan.nextInt();
			t.d = scan.nextDouble();
			if (scan.hasNextLine())
				scan.nextLine();
			a.add(t);
		}
		System.out.println("NON-STOCK items; sorry!");
		for (int c = 0; c < a.size(); c++) {
			if (a.get(c).q < 1 && a.get(c).d > 0)
				System.out.println(a.get(c).n);
		}
		ArrayList<TFS_1> ts = new ArrayList<>();
		for (int c = 0; c < a.size(); c++) {
			if (a.get(c).q > 0 && a.get(c).d > 0)
				ts.add(a.get(c));
		}
		System.out.println("\n\nIN-STOCK items;");
		for (int c = 0; c < ts.size(); c++) {
			System.out.println((c + 1) + ":" + ts.get(c).n+" ("+ts.get(c).q+")");
		}
		Scanner scan2 = new Scanner(System.in);
		System.out.println("Please choose an item to add to your basket (0 for nothing):");
		int u = scan2.nextInt();
		scan2.nextLine();
		if (u > 0) {
			ts.get(u - 1).q--;
			System.out.println("bought item " + ts.get(u - 1).n);
			System.out.println("there are " + ts.get(u - 1).q + " left");
		}
		FileWriter fw = new FileWriter("stock.txt");
		int q2 = 0;
		double d2 = 0;
		for (TFS_1 thing : a) {
			q2 += thing.q;
			d2 += (thing.d * thing.q);
			fw.write(thing.n + "\n");
			fw.write(thing.q + " " + thing.d + "\n");
		}
		if (u > 0)
			System.out.println("You bought " + String.format("�%.02f", ts.get(u - 1).d) + " worth of stock");
		System.out.println("there are still " + q2 + " items left in the store, worth " + String.format("�%.02f", d2)
				+ " in total");
		scan.close();
		scan2.close();
		fw.close();

	}

}
