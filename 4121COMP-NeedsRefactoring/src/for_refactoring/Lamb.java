package for_refactoring;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * A simple data object representing a Lamb
 * 
 * @author david
 */
public class Lamb {
	/**
	 * A Gender Identity for Sheep and Lambs. RAM (male) or EWE (female)
	 */
	public enum Gender {
		RAM, EWE
	};

	/**
	 * The Gender identity of this Lamb. As per values found within
	 * {@link Gender}; can also be null
	 */
	public Gender gender;
	/**
	 * Indicates if the Lamb is Woolly - true if so, false if shorn
	 */
	public boolean woolly;
	/**
	 * Indicates the name of the Lamb.
	 */
	public String name;

	/**
	 * Constructs a new Lamb object with the specified parameters
	 * 
	 * @param name
	 *            the Lamb's name
	 * @param wool
	 *            true if the lamb is woolly; false if they've been shorn
	 * @param gender
	 *            the applicable Gender identity for the Lamb
	 */
	public Lamb(String name, boolean wool, Gender gender) {
		this.name = name;
		this.woolly = wool;
		this.gender = gender;
	}
	
	/**
	 * Sorts it all out and makes it work
	 * @param args ignored
	 * @throws FileNotFoundException if the file is not found
	 */
	public static void main(String[] args) throws FileNotFoundException {

		FileInputStream stream = new FileInputStream("lambs.txt");
		ArrayList<Lamb> flock = new ArrayList<>();

		Scanner scan = new Scanner(stream);

		while (scan.hasNextLine()) {
			Lamb newLamb = new Lamb(scan.nextLine(), false, null);
			newLamb.gender = Lamb.Gender.valueOf(scan.nextLine().trim());

			// This line's been added just so we can see the wide-ranging
			// effects of an Extract Constant
			String soWoolly = "woolly";

			String woollyness = scan.nextLine();
			newLamb.woolly = false;
			if (woollyness.equals("woolly"))
				newLamb.woolly = true;

			flock.add(newLamb);
		}
		scan.close();

		Collections.sort(flock, new Comparator<Lamb>() {
			@Override
			public int compare(Lamb o1, Lamb o2) {
				if (o1.woolly && !o2.woolly)
					return -1;
				else if (o1.woolly && o2.woolly)
					return 0;
				else
					return 1;
			}
		});

		System.out.println("Lambs--");
		for (int i=0; i< flock.size(); i++) {
			if (flock.get(i).woolly) {
				System.out.print("Wooly: ");
			} else
				System.out.print("Shorn: ");
			System.out.println("Lamb "+i+" "+flock.get(i).name + " (" + flock.get(i).gender + ")");
		}
	}

}
